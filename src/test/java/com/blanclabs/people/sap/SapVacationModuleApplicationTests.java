package com.blanclabs.people.sap;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.StringUtils;

import com.blanclabs.people.sap.model.VacationRequestDTO;
import com.blanclabs.people.sap.repository.RequestRepository;
import com.blanclabs.people.sap.repository.entity.RequestStatus;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc(secure = false)
@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
public class SapVacationModuleApplicationTests {

   private static final long ID = 1;

   @Autowired
   private MockMvc mvc;

   @Autowired
   private RequestRepository requestRepository;

   @Test
   @WithMockUser(username = "admin", password = "admin")
   public void getAllRequestTest() throws Exception {
      final List<VacationRequestDTO> requestDb = requestRepository.getAll();
      mvc.perform(get("/vacation")).andExpect(status().isOk()).andExpect(r -> assertEquals("Missmatch request result with actual result",
            requestDb.size(), StringUtils.countOccurrencesOf(r.getResponse().getContentAsString(), "title")));
   }

   @Test
   @WithMockUser(username = "admin", password = "admin")
   public void getOneRequestTest() throws Exception {
      final Optional<VacationRequestDTO> requestDb = requestRepository.getById(ID);
      mvc.perform(get(String.format("/vacation/%s", ID))).andExpect(status().isOk())
            .andExpect(r -> assertTrue(
                  String.format("Missmatch request description %s => %s", requestDb.get().getDescription(),
                        r.getResponse().getContentAsString()),
                  r.getResponse().getContentAsString().contains(requestDb.get().getDescription())));
   }

   @Test
   @WithMockUser(username = "admin", password = "admin")
   public void getOneUpdatedStatusTest() throws Exception {
      mvc.perform(patch(String.format("/vacation/%s", ID)).content(RequestStatus.ACCEPTED.toString())).andExpect(status().isCreated());
      final Optional<VacationRequestDTO> requestDb = requestRepository.getById(ID);
      assertTrue(requestDb.get().getStatus().equals(RequestStatus.ACCEPTED));
   }

}
