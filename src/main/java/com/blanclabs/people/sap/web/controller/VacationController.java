package com.blanclabs.people.sap.web.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.blanclabs.people.sap.model.VacationRequestDTO;
import com.blanclabs.people.sap.repository.RequestRepository;
import com.blanclabs.people.sap.repository.entity.RequestStatus;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping(path = "/vacation")
public class VacationController {

   @Autowired
   private RequestRepository requestRepository;

   @CrossOrigin(origins = "*")
   @GetMapping
   public List<VacationRequestDTO> getAllVacationRequest() {
      log.info("Retrieving all vacation request");
      return requestRepository.getAll();
   }

   @CrossOrigin(origins = "*")
   @GetMapping(path = "/{id}")
   public ResponseEntity<VacationRequestDTO> getVacationRequest(@PathVariable(name = "id", required = true) final String id) {
      log.info("Retrieved vacation request for {}", id);
      try {
         final Optional<VacationRequestDTO> dto = requestRepository.getById(Long.valueOf(id));
         if (dto.isPresent()) {
            return ResponseEntity.ok(dto.get());
         }
      } catch (NumberFormatException e) {
         return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
      }
      return ResponseEntity.notFound().build();
   }

   @CrossOrigin(origins = "*")
   @PatchMapping(path = "/{id}")
   @Transactional
   public ResponseEntity<Void> updateRequest(@PathVariable(name = "id", required = true) final String id,
         @RequestBody(required = true) final String status) {
      log.info("Updating request {} with status {}", id, status);
      try {
         requestRepository.updateStatus(RequestStatus.valueOf(status), Long.valueOf(id));
         final URI location = ServletUriComponentsBuilder.fromCurrentRequest().buildAndExpand().toUri();
         return ResponseEntity.created(location).build();
      } catch (NumberFormatException e) {
         return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
      }
   }

}
