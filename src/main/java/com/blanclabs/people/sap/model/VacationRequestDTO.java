package com.blanclabs.people.sap.model;

import java.time.LocalDateTime;

import com.blanclabs.people.sap.repository.entity.RequestStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@AllArgsConstructor
@RequiredArgsConstructor
@Builder
@Getter
@ToString
public class VacationRequestDTO {

   private final long id;
   private final LocalDateTime startDate;
   private final LocalDateTime endDate;
   private final String title;
   private final String employee;
   private final RequestStatus status;
   private long employeeId;
   private LocalDateTime createdDate;
   private String description;
}
