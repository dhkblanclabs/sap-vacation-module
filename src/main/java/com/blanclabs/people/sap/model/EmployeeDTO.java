package com.blanclabs.people.sap.model;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Builder
@Getter
@ToString
public class EmployeeDTO {
   private final long id;
   private final String name;
}
