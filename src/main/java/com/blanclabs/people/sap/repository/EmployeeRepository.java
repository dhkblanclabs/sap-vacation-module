package com.blanclabs.people.sap.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.blanclabs.people.sap.repository.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
