package com.blanclabs.people.sap.repository.entity;

public enum RequestStatus {

   OPEN, ACCEPTED, REJECTED;
}
