package com.blanclabs.people.sap.repository.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Table(name = "VAC_REQUIREMENT")
@Entity
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class VacationRequest implements Serializable {

   private static final long serialVersionUID = -7850824859894111037L;

   @Id
   @Column(name = "ID", unique = true, nullable = false, length = 36)
   @GeneratedValue(generator = "VAC_REQUIREMENT_SEQUENCE")
   @SequenceGenerator(name = "VAC_REQUIREMENT_SEQUENCE", sequenceName = "VAC_REQ_SEQ", allocationSize = 1)
   private Long id;

   @Column(name = "START_DATE")
   private LocalDateTime startDate;

   @Column(name = "END_DATE")
   private LocalDateTime endDate;

   @Column(name = "CREATED_DATE")
   private LocalDateTime createdDate;

   @Column(name = "TITLE", length = 30)
   private String title;

   @Column(name = "DESCRIPTION", length = 300)
   private String description;

   @Enumerated(EnumType.STRING)
   @Column(name = "STATUS", length = 10)
   private RequestStatus status;

   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "EMP_ID", nullable = false)
   private Employee employee;
}
