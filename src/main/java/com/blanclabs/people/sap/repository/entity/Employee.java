package com.blanclabs.people.sap.repository.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Table(name = "EMPLOYEE")
@Entity
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Employee implements Serializable {

   private static final long serialVersionUID = 4412659440777542319L;

   @Id
   @Column(name = "ID", unique = true, nullable = false, length = 36)
   @GeneratedValue(generator = "EMPLOYEE_SEQUENCE")
   @SequenceGenerator(name = "EMPLOYEE_SEQUENCE", sequenceName = "EMP_SEQ", allocationSize = 1)
   private Long id;

   @Column(name = "NAME")
   private String name;
}
