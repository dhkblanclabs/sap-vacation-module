package com.blanclabs.people.sap.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.blanclabs.people.sap.model.VacationRequestDTO;
import com.blanclabs.people.sap.repository.entity.RequestStatus;
import com.blanclabs.people.sap.repository.entity.VacationRequest;

public interface RequestRepository extends JpaRepository<VacationRequest, Long> {

   @Query("select new com.blanclabs.people.sap.model.VacationRequestDTO( r.id, r.startDate, r.endDate, r.title, e.name, r.status )"
         + " from VacationRequest r join r.employee e")
   List<VacationRequestDTO> getAll();

   @Query("select new com.blanclabs.people.sap.model.VacationRequestDTO( "
         + "r.id, r.startDate, r.endDate, r.title, e.name, r.status, e.id, r.createdDate, r.description)"
         + " from VacationRequest r join r.employee e where r.id = ?1 ")
   Optional<VacationRequestDTO> getById(final Long id);

   @Modifying
   @Query("update VacationRequest r set r.status = ?1 where r.id = ?2")
   void updateStatus(final RequestStatus status, final Long id);

}
