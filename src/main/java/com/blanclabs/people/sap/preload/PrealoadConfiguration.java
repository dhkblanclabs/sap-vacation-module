package com.blanclabs.people.sap.preload;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.blanclabs.people.sap.repository.EmployeeRepository;
import com.blanclabs.people.sap.repository.RequestRepository;
import com.blanclabs.people.sap.repository.entity.Employee;
import com.blanclabs.people.sap.repository.entity.RequestStatus;
import com.blanclabs.people.sap.repository.entity.VacationRequest;

@Component
public class PrealoadConfiguration implements CommandLineRunner {

   private static final int MIN_START = 0;
   private static final int MAX_START = 5;
   private static final int MIN_END = 10;
   private static final int MAX_END = 20;

   @Autowired
   private EmployeeRepository employeeRepository;

   @Autowired
   private RequestRepository requestRepository;

   @Override
   public void run(String... args) throws Exception {
      final List<Employee> employees = employeeRepository.saveAll(prepareEmployees());
      employees.forEach(e -> requestRepository.saveAll(prepareRequests(e)));
   }

   private List<Employee> prepareEmployees() {
      return Arrays.asList(Employee.builder().name("Monic").build(), Employee.builder().name("Gustavo").build(),
            Employee.builder().name("Alejandri").build(), Employee.builder().name("Luis").build(),
            Employee.builder().name("Adolfo").build(), Employee.builder().name("Jake").build());
   }

   private List<VacationRequest> prepareRequests(final Employee employee) {

      return Arrays.asList(
            VacationRequest.builder().createdDate(LocalDateTime.now()).description("Taking vacations for a while")
                  .endDate(LocalDateTime.now().plusDays(ThreadLocalRandom.current().nextLong(MIN_END, MAX_END)))
                  .startDate(LocalDateTime.now().plusDays(ThreadLocalRandom.current().nextLong(MIN_START, MAX_START)))
                  .title("Year vacations").employee(employee).status(RequestStatus.OPEN).build(),
            VacationRequest.builder().createdDate(LocalDateTime.now()).description("I need to spend so mquality time with my family")
                  .endDate(LocalDateTime.now().plusDays(ThreadLocalRandom.current().nextLong(MIN_END, MAX_END)))
                  .startDate(LocalDateTime.now().plusDays(ThreadLocalRandom.current().nextLong(MIN_START, MAX_START)))
                  .title("Family vacations").employee(employee).status(RequestStatus.OPEN).build(),
            VacationRequest.builder().createdDate(LocalDateTime.now()).description("Personal stuf need to be resolved")
                  .endDate(LocalDateTime.now().plusDays(ThreadLocalRandom.current().nextLong(MIN_END, MAX_END)))
                  .startDate(LocalDateTime.now().plusDays(ThreadLocalRandom.current().nextLong(MIN_START, MAX_START)))
                  .title("Personal vacations").employee(employee).status(RequestStatus.OPEN).build());
   }
}
