package com.blanclabs.people.sap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(exclude = { org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class })
public class SapVacationModuleApplication {

   public static void main(String[] args) {
      SpringApplication.run(SapVacationModuleApplication.class, args);
   }
}
